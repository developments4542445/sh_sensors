# Introduction: #
This is the project associated with the QA Automation for commissioning and decommissioning of the sensors associated to the SGW and the mGW.

It will use several sensors that can be founded in the next Confluence link:
http://192.168.5.159:8090/confluence/display/PRODDEV/220C2003+-+%5BProd+Def%5D+Supported+devices

-----

### Files distribution: ###
- Over folder *Python* are the Python files required to this project.
- Over folder *Arduino* are the Arduino files required to this project.

----

### Libraries required in Python: ###
- sys
- serial
- time

----

### Lists of sensors supported: ###
- pir sensors
- door lock sensors
----

### Lists of char used by serial port: ###

Sensors | Commissioning Code | Decommissioning Code
---       | --- | ---
Pir       | 'a'   | 'b'
Door lock | 'c'   | 'd'
