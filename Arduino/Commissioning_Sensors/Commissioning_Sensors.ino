#define LED 13
#define DOORLOCK_SENSOR 12
#define PIR_SENSOR 11

void setup() {
  Serial.begin(9600);
  pinMode(LED, LOW);
  pinMode(LED, OUTPUT);
  pinMode(PIR_SENSOR, LOW);
  pinMode(PIR_SENSOR, OUTPUT);
  pinMode(DOORLOCK_SENSOR, LOW);
  pinMode(DOORLOCK_SENSOR, OUTPUT);

}

void loop() {
  char input = Serial.read();

  switch (input)
  {
    case 'a':                                                                                 //Commissioning of a pir sensor
      Serial.println("Commissioning of a pir sensor has started.");
      digitalWrite(PIR_SENSOR, HIGH);
      delay(1000);
      digitalWrite(PIR_SENSOR, LOW);
    break;

    case 'b':                                                                                 //Decommissioning of a pir sensor
      Serial.println("Decommissioning of a pir sensor has started.");
      digitalWrite(PIR_SENSOR, HIGH);
      delay(1000);
      digitalWrite(PIR_SENSOR, LOW);
      delay(500);
      digitalWrite(PIR_SENSOR, HIGH);
      delay(5000);
      digitalWrite(PIR_SENSOR, LOW);
    break;

    case 'c':                                                                                 //Commissioning of a door lock sensor
      Serial.println("Commissioning of a doorlock sensor has started.");
      digitalWrite(DOORLOCK_SENSOR, HIGH);
      delay(1000);
      digitalWrite(DOORLOCK_SENSOR, LOW);
    break;

    case 'd':                                                                                 //Decommissioning of a door lock sensor
      Serial.println("Decommissioning of a doorlock sensor has started.");
      digitalWrite(DOORLOCK_SENSOR, HIGH);
      delay(1000);
      digitalWrite(DOORLOCK_SENSOR, LOW);
      delay(100);
      digitalWrite(DOORLOCK_SENSOR, HIGH);
      delay(5000);
      digitalWrite(DOORLOCK_SENSOR, LOW);
    break;

    default:
      Serial.println("Default line. There is no action generated.");
    break;
  }
}
