# #!/usr/bin/python3

import sys
import serial
import time

# command to visualise the commissioning in the SmartGateway
# mosquitto_sub -v -h localhost -p 1883 -t '#'

# Next program can open a serial port in a specific place and send commands.
# For this situation, it sends 'a' for commissioning a pir sensor and 'b' por decommissioning
# For this situation, it sends 'c' for commissioning a door lock sensor and 'd' por decommissioning

if __name__ == '__main__':
    print('Generate a commissioning or decommissioning.')
    print('Perform pir sensor commissioning (a) or decommissioning (b).')
    print('Perform door lock sensor commissioning (c) or decommissioning (d).')
    command_to_perform = input()

    serial_commissioning = serial.Serial(port='/dev/ttyACM0', baudrate=9600, bytesize=serial.EIGHTBITS,
                                         parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=1,
                                         xonxoff=False, rtscts=False, dsrdtr=False, writeTimeout=2)
    time.sleep(5)

    # serial_commissioning.open()
    serial_commissioning.flushInput()
    serial_commissioning.flushOutput()
    serial_commissioning.write(bytearray(command_to_perform, 'ascii'))
    response = serial_commissioning.readline()
    print('read data: ' + str(response))
    serial_commissioning.close()

sys.exit()

